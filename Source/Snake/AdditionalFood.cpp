// Fill out your copyright notice in the Description page of Project Settings.


#include "AdditionalFood.h"
#include "SnakeBase.h"
#include "Food.h"
#include "Interactable.h"

// Sets default values
AAdditionalFood::AAdditionalFood()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	FoodRespQuantity = 4;
	minX = -380;
	maxX = 760;
	minY = -770;
	maxY = 1540;

}

// Called when the game starts or when spawned
void AAdditionalFood::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void AAdditionalFood::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

// spawn food in random places across the field
void AAdditionalFood::Interact(AActor* Interactor, bool bIsHead)
{
	if (bIsHead)
	{
		auto Snake = Cast<ASnakeBase>(Interactor);
		if (IsValid(Snake))
		{
			for (int i = 0; i < FoodRespQuantity; ++i)
			{
				FVector NewLocation(rand() % maxX + minX, rand() % maxY + minY, 0);
				FTransform NewTransform(NewLocation);
				AFood* NewFood = GetWorld()->SpawnActor<AFood>(FoodClass, NewTransform);
			}
			this->Destroy();
		}
	}
}


