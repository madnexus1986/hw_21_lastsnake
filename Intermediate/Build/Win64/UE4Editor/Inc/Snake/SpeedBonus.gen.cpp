// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "Snake/SpeedBonus.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeSpeedBonus() {}
// Cross Module References
	SNAKE_API UClass* Z_Construct_UClass_ASpeedBonus_NoRegister();
	SNAKE_API UClass* Z_Construct_UClass_ASpeedBonus();
	ENGINE_API UClass* Z_Construct_UClass_AActor();
	UPackage* Z_Construct_UPackage__Script_Snake();
	SNAKE_API UClass* Z_Construct_UClass_UInteractable_NoRegister();
// End Cross Module References
	void ASpeedBonus::StaticRegisterNativesASpeedBonus()
	{
	}
	UClass* Z_Construct_UClass_ASpeedBonus_NoRegister()
	{
		return ASpeedBonus::StaticClass();
	}
	struct Z_Construct_UClass_ASpeedBonus_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_MovementSpeedBonus_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_MovementSpeedBonus;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FImplementedInterfaceParams InterfaceParams[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_ASpeedBonus_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_AActor,
		(UObject* (*)())Z_Construct_UPackage__Script_Snake,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ASpeedBonus_Statics::Class_MetaDataParams[] = {
		{ "IncludePath", "SpeedBonus.h" },
		{ "ModuleRelativePath", "SpeedBonus.h" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ASpeedBonus_Statics::NewProp_MovementSpeedBonus_MetaData[] = {
		{ "Category", "SpeedBonus" },
		{ "ModuleRelativePath", "SpeedBonus.h" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_ASpeedBonus_Statics::NewProp_MovementSpeedBonus = { "MovementSpeedBonus", nullptr, (EPropertyFlags)0x0010000000010001, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ASpeedBonus, MovementSpeedBonus), METADATA_PARAMS(Z_Construct_UClass_ASpeedBonus_Statics::NewProp_MovementSpeedBonus_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_ASpeedBonus_Statics::NewProp_MovementSpeedBonus_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_ASpeedBonus_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ASpeedBonus_Statics::NewProp_MovementSpeedBonus,
	};
		const UE4CodeGen_Private::FImplementedInterfaceParams Z_Construct_UClass_ASpeedBonus_Statics::InterfaceParams[] = {
			{ Z_Construct_UClass_UInteractable_NoRegister, (int32)VTABLE_OFFSET(ASpeedBonus, IInteractable), false },
		};
	const FCppClassTypeInfoStatic Z_Construct_UClass_ASpeedBonus_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<ASpeedBonus>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_ASpeedBonus_Statics::ClassParams = {
		&ASpeedBonus::StaticClass,
		"Engine",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_ASpeedBonus_Statics::PropPointers,
		InterfaceParams,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_ASpeedBonus_Statics::PropPointers),
		UE_ARRAY_COUNT(InterfaceParams),
		0x009000A4u,
		METADATA_PARAMS(Z_Construct_UClass_ASpeedBonus_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_ASpeedBonus_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_ASpeedBonus()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_ASpeedBonus_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(ASpeedBonus, 2445639081);
	template<> SNAKE_API UClass* StaticClass<ASpeedBonus>()
	{
		return ASpeedBonus::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_ASpeedBonus(Z_Construct_UClass_ASpeedBonus, &ASpeedBonus::StaticClass, TEXT("/Script/Snake"), TEXT("ASpeedBonus"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(ASpeedBonus);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
