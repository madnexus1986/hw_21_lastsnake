// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef SNAKE_AdditionalFood_generated_h
#error "AdditionalFood.generated.h already included, missing '#pragma once' in AdditionalFood.h"
#endif
#define SNAKE_AdditionalFood_generated_h

#define Snake_Source_Snake_AdditionalFood_h_14_SPARSE_DATA
#define Snake_Source_Snake_AdditionalFood_h_14_RPC_WRAPPERS
#define Snake_Source_Snake_AdditionalFood_h_14_RPC_WRAPPERS_NO_PURE_DECLS
#define Snake_Source_Snake_AdditionalFood_h_14_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesAAdditionalFood(); \
	friend struct Z_Construct_UClass_AAdditionalFood_Statics; \
public: \
	DECLARE_CLASS(AAdditionalFood, AActor, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/Snake"), NO_API) \
	DECLARE_SERIALIZER(AAdditionalFood) \
	virtual UObject* _getUObject() const override { return const_cast<AAdditionalFood*>(this); }


#define Snake_Source_Snake_AdditionalFood_h_14_INCLASS \
private: \
	static void StaticRegisterNativesAAdditionalFood(); \
	friend struct Z_Construct_UClass_AAdditionalFood_Statics; \
public: \
	DECLARE_CLASS(AAdditionalFood, AActor, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/Snake"), NO_API) \
	DECLARE_SERIALIZER(AAdditionalFood) \
	virtual UObject* _getUObject() const override { return const_cast<AAdditionalFood*>(this); }


#define Snake_Source_Snake_AdditionalFood_h_14_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API AAdditionalFood(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(AAdditionalFood) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AAdditionalFood); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AAdditionalFood); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AAdditionalFood(AAdditionalFood&&); \
	NO_API AAdditionalFood(const AAdditionalFood&); \
public:


#define Snake_Source_Snake_AdditionalFood_h_14_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AAdditionalFood(AAdditionalFood&&); \
	NO_API AAdditionalFood(const AAdditionalFood&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AAdditionalFood); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AAdditionalFood); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(AAdditionalFood)


#define Snake_Source_Snake_AdditionalFood_h_14_PRIVATE_PROPERTY_OFFSET
#define Snake_Source_Snake_AdditionalFood_h_11_PROLOG
#define Snake_Source_Snake_AdditionalFood_h_14_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Snake_Source_Snake_AdditionalFood_h_14_PRIVATE_PROPERTY_OFFSET \
	Snake_Source_Snake_AdditionalFood_h_14_SPARSE_DATA \
	Snake_Source_Snake_AdditionalFood_h_14_RPC_WRAPPERS \
	Snake_Source_Snake_AdditionalFood_h_14_INCLASS \
	Snake_Source_Snake_AdditionalFood_h_14_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Snake_Source_Snake_AdditionalFood_h_14_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Snake_Source_Snake_AdditionalFood_h_14_PRIVATE_PROPERTY_OFFSET \
	Snake_Source_Snake_AdditionalFood_h_14_SPARSE_DATA \
	Snake_Source_Snake_AdditionalFood_h_14_RPC_WRAPPERS_NO_PURE_DECLS \
	Snake_Source_Snake_AdditionalFood_h_14_INCLASS_NO_PURE_DECLS \
	Snake_Source_Snake_AdditionalFood_h_14_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> SNAKE_API UClass* StaticClass<class AAdditionalFood>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Snake_Source_Snake_AdditionalFood_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
