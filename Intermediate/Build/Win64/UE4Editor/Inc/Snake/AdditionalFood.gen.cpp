// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "Snake/AdditionalFood.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeAdditionalFood() {}
// Cross Module References
	SNAKE_API UClass* Z_Construct_UClass_AAdditionalFood_NoRegister();
	SNAKE_API UClass* Z_Construct_UClass_AAdditionalFood();
	ENGINE_API UClass* Z_Construct_UClass_AActor();
	UPackage* Z_Construct_UPackage__Script_Snake();
	COREUOBJECT_API UClass* Z_Construct_UClass_UClass();
	SNAKE_API UClass* Z_Construct_UClass_AFood_NoRegister();
	ENGINE_API UClass* Z_Construct_UClass_UStaticMeshComponent_NoRegister();
	SNAKE_API UClass* Z_Construct_UClass_UInteractable_NoRegister();
// End Cross Module References
	void AAdditionalFood::StaticRegisterNativesAAdditionalFood()
	{
	}
	UClass* Z_Construct_UClass_AAdditionalFood_NoRegister()
	{
		return AAdditionalFood::StaticClass();
	}
	struct Z_Construct_UClass_AAdditionalFood_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_FoodClass_MetaData[];
#endif
		static const UE4CodeGen_Private::FClassPropertyParams NewProp_FoodClass;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_FoodRespQuantity_MetaData[];
#endif
		static const UE4CodeGen_Private::FUnsizedIntPropertyParams NewProp_FoodRespQuantity;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_MeshComponent_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_MeshComponent;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FImplementedInterfaceParams InterfaceParams[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_AAdditionalFood_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_AActor,
		(UObject* (*)())Z_Construct_UPackage__Script_Snake,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AAdditionalFood_Statics::Class_MetaDataParams[] = {
		{ "IncludePath", "AdditionalFood.h" },
		{ "ModuleRelativePath", "AdditionalFood.h" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AAdditionalFood_Statics::NewProp_FoodClass_MetaData[] = {
		{ "Category", "AdditionalFood" },
		{ "ModuleRelativePath", "AdditionalFood.h" },
	};
#endif
	const UE4CodeGen_Private::FClassPropertyParams Z_Construct_UClass_AAdditionalFood_Statics::NewProp_FoodClass = { "FoodClass", nullptr, (EPropertyFlags)0x0014000000010001, UE4CodeGen_Private::EPropertyGenFlags::Class, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(AAdditionalFood, FoodClass), Z_Construct_UClass_AFood_NoRegister, Z_Construct_UClass_UClass, METADATA_PARAMS(Z_Construct_UClass_AAdditionalFood_Statics::NewProp_FoodClass_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_AAdditionalFood_Statics::NewProp_FoodClass_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AAdditionalFood_Statics::NewProp_FoodRespQuantity_MetaData[] = {
		{ "Category", "AdditionalFood" },
		{ "ModuleRelativePath", "AdditionalFood.h" },
	};
#endif
	const UE4CodeGen_Private::FUnsizedIntPropertyParams Z_Construct_UClass_AAdditionalFood_Statics::NewProp_FoodRespQuantity = { "FoodRespQuantity", nullptr, (EPropertyFlags)0x0010000000010001, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(AAdditionalFood, FoodRespQuantity), METADATA_PARAMS(Z_Construct_UClass_AAdditionalFood_Statics::NewProp_FoodRespQuantity_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_AAdditionalFood_Statics::NewProp_FoodRespQuantity_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AAdditionalFood_Statics::NewProp_MeshComponent_MetaData[] = {
		{ "Category", "AdditionalFood" },
		{ "EditInline", "true" },
		{ "ModuleRelativePath", "AdditionalFood.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_AAdditionalFood_Statics::NewProp_MeshComponent = { "MeshComponent", nullptr, (EPropertyFlags)0x00100000000a001d, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(AAdditionalFood, MeshComponent), Z_Construct_UClass_UStaticMeshComponent_NoRegister, METADATA_PARAMS(Z_Construct_UClass_AAdditionalFood_Statics::NewProp_MeshComponent_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_AAdditionalFood_Statics::NewProp_MeshComponent_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_AAdditionalFood_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AAdditionalFood_Statics::NewProp_FoodClass,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AAdditionalFood_Statics::NewProp_FoodRespQuantity,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AAdditionalFood_Statics::NewProp_MeshComponent,
	};
		const UE4CodeGen_Private::FImplementedInterfaceParams Z_Construct_UClass_AAdditionalFood_Statics::InterfaceParams[] = {
			{ Z_Construct_UClass_UInteractable_NoRegister, (int32)VTABLE_OFFSET(AAdditionalFood, IInteractable), false },
		};
	const FCppClassTypeInfoStatic Z_Construct_UClass_AAdditionalFood_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<AAdditionalFood>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_AAdditionalFood_Statics::ClassParams = {
		&AAdditionalFood::StaticClass,
		"Engine",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_AAdditionalFood_Statics::PropPointers,
		InterfaceParams,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_AAdditionalFood_Statics::PropPointers),
		UE_ARRAY_COUNT(InterfaceParams),
		0x009000A4u,
		METADATA_PARAMS(Z_Construct_UClass_AAdditionalFood_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_AAdditionalFood_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_AAdditionalFood()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_AAdditionalFood_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(AAdditionalFood, 1137864465);
	template<> SNAKE_API UClass* StaticClass<AAdditionalFood>()
	{
		return AAdditionalFood::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_AAdditionalFood(Z_Construct_UClass_AAdditionalFood, &AAdditionalFood::StaticClass, TEXT("/Script/Snake"), TEXT("AAdditionalFood"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(AAdditionalFood);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
